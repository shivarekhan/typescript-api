import { Request, Response } from "express";
import query from "../model/queries";
import pool from "../model/connection";
import Joi from "joi";

const updateProduct = (req: Request, res: Response) => {
  const schema = Joi.object()
    .keys({
      id: Joi.number().required(),
      title: Joi.string().required(),
      price: Joi.number().integer().required(),
      category: Joi.string().required(),
    })
    .unknown(false);
  const { error } = schema.validate(req.body);

  if (error) {
    const { details } = error;
    res.status(200).json({ ERROR: details });
  } else {
    const { id, title, price, category } = req.body;
    console.log(req.body, title);
    pool.query(query.updateEntry,[title, price, category, id],(error, data) => {
        if (!error) {
          res.status(200).json({ SUCCESS: `Product updated successfully.` });
        }
      }
    );
  }
};

export default updateProduct;
