import pool from "../model/connection";
import { Request, Response } from "express";
import query from "../model/queries";

const deleteProduct = (req: Request, res: Response) => {
  const id: number = parseInt(req.params.id);
  console.log(id);
  pool.query(query.checkProductViaId, [id], (error, data) => {
    console.log(data.rows.length);
    if (data.rows.length) {
      pool.query(query.deleteViaId, [id], (error, data) => {
        if (error) {
          res.status(400).json({ ERROR: "Product Not Removed" });
        } else {
          res.status(200).json({ SUCCESS: "Product Removed Successfully" });
        }
      });
    } else {
      res.status(400).json({ ERROR: "Product with given id not found" });
    }
  });
};

export default deleteProduct;
