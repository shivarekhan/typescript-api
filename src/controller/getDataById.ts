import pool from "../model/connection";
import { Request, Response } from "express";
import query from "../model/queries";
import Joi from "joi";

const idSchema = Joi.object({
  id: Joi.number().integer().required(),
});

const getProductById = (req: Request, res: Response) => {
  const id: number = parseInt(req.params.id);
  pool.query(query.getProductByIdQuery, [id], (error, data) => {
    if (!error) {
      res.status(200).json(data.rows);
    } else {
      res.status(404).json({ ERROR: "product with given id not found" });
    }
  });
};

export default getProductById;
