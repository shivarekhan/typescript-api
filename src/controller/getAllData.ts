import pool from "../model/connection";
import { Request, Response } from "express";
import query from "../model/queries";

const getAllData = (req: Request, res: Response) => {
  pool.query(query.getAllData, (error, data) => {
    if (!error) {
      res.status(200).json(data.rows);
    } else {
      res.status(400).json({ ERROR: "No products found at this moment." });
    }
  });
};

export default getAllData;
