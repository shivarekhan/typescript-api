import pool from "../model/connection";
import { Request, Response } from "express";
import query from "../model/queries";
import Joi from "joi";

const addProduct = (req: Request, res: Response) => {
  const schema = Joi.object()
    .keys({
      title: Joi.string().required(),
      price: Joi.number().integer().required(),
      category: Joi.string().required(),
    })
    .unknown(false);
  const { error } = schema.validate(req.body);

  if (error) {
    const { details } = error;
    res.status(200).json({ ERROR: details });
  } else {
    const { title, price, category } = req.body;
    console.log(req.body, title);
    pool.query(query.insertProduct, [title, price, category], (error, data) => {
      if (!error) {
        res.status(200).json({ SUCCESS: `Product added successfully.` });
      }
    });
  }
};

export default addProduct;
