const query = {
  getAllData: `SELECT * FROM fakeproducts`,
  getProductByIdQuery: "SELECT * FROM fakeproducts WHERE id = $1",
  checkProductViaId: "SELECT entry FROM fakeproducts entry WHERE id=$1",
  insertProduct: `INSERT INTO fakeproducts(title,price,category) VALUES ($1,$2,$3)`,
  deleteViaId: "DELETE FROM fakeproducts WHERE id = $1",
  maxId: "SELECT max(productId) FROM fakeproducts",
  updateEntry:
    "UPDATE fakeproducts SET title=$1,price=$2,category=$3 WHERE id = $4 ",
};
export default query;
