import { Pool } from "pg";
import { password, user, database, host } from "../config/config"

const pool = new Pool({
  host: host,
  user: user,
  password: password,
  database: database,
});

export default pool;
