import express, { Express, Request, Response } from "express";
import cors from "cors";
import getProductRouter from "./router/getProductRouter";
import addProductRouter from "./controller/addProduct";
import deleteProductRouter from "./router/deleteProductRouter";
import updateProductRouter from "./router/updateProductRouter";
const app: Express = express();
const port: number | string = process.env.PORT || 5000;

app.use(express.json());
app.use(cors());

app.get("/", (req: Request, res: Response) => {
  res.status(200).json({ SUCCESS: "REST_API using typescript and nodejs" });
});

app.use("/getProducts", getProductRouter);
app.use("/addProduct", addProductRouter);
app.use("/deleteProduct", deleteProductRouter);
app.use("/updateProduct", updateProductRouter);

app.listen(port, () => {
  console.log(`server is running on port ${port}`);
});
