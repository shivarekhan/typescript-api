import { Request, Response, Router } from "express";
import updateProduct from "../controller/updateProduct";

const updateProductRouter: Router = Router();

updateProductRouter.post('/',updateProduct);

export default updateProductRouter;
