import { Router, Request, Response } from "express";
import addProduct from "../controller/addProduct";

const addProductRouter: Router = Router();

addProductRouter.post("/", addProduct);

export default addProductRouter;
