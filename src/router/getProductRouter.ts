import { Router, Request, Response } from "express";
import getAllData from "../controller/getAllData";
import getDataById from "../controller/getDataById";
import Joi from "joi";

const getProductRouter: Router = Router();

const validationMiddleware = (req: Request, res: Response, next: Function) => {
  const schema = Joi.object({
    id: Joi.number().required(),
  }).unknown(false);

  const { error } = schema.validate({ id: req.params.id });
  if (error) {
    const { details } = error;
    res.status(200).json({ error: details });
  } else {
    next();
  }
};

getProductRouter.get("/", getAllData);
getProductRouter.get("/:id", validationMiddleware, getDataById);

export default getProductRouter;
