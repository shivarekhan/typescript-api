import { Router, Request, Response } from "express";
import deleteProduct from "../controller/deleteProduct";
import Joi from "joi";

const deleteProductRouter: Router = Router();

const validationMiddleware = (req: Request, res: Response, next: Function) => {
  const schema = Joi.object({
    id: Joi.number().required(),
  }).unknown(false);

  const { error } = schema.validate({ id: req.params.id });
  if (error) {
    const { details } = error;
    res.status(200).json({ error: details });
  } else {
    next();
  }
};

deleteProductRouter.delete("/:id", validationMiddleware, deleteProduct);

export default deleteProductRouter;
